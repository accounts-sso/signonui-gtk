#!/bin/sh -ea

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

aclocal #-I m4
autoheader
automake --add-missing --copy
autoconf
autoreconf --install --force
. $srcdir/configure "$@"


