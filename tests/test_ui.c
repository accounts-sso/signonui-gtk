/* vi: set et sw=4 ts=4 cino=t0,(0: */
/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This file is part of gsignond
 *
 * Copyright (C) 2012 Intel Corporation.
 *
 * Contact: Amarnaht Valluri <amarnath.valluri@linux.intel.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "config.h"
#include <check.h>
#include <error.h>
#include <errno.h>
#include <stdlib.h>
#include <gio/gio.h>
#include <glib.h>
#include <string.h>
#include <unistd.h>

#include "gsso-ui-dialog-dbus-glue.h"
#include "gsso-ui-dbus-glue.h"
#include "gsso-ui-utils.h"
#include "gsso-ui-types.h"

#if HAVE_GTESTDBUS
GTestDBus *dbus = NULL;
#else
GPid daemon_pid = 0;
#endif

static void
setup_daemon (void)
{
    fail_if (g_setenv ("G_MESSAGES_DEBUG", "all", TRUE) == FALSE);

#if HAVE_GTESTDBUS
    dbus = g_test_dbus_new (G_TEST_DBUS_NONE);
    fail_unless (dbus != NULL, "could not create test dbus");

    g_test_dbus_add_service_dir (dbus, DBUS_SERVICE_DIR);

    g_test_dbus_up (dbus);
    g_print ("Test dbus server address : %s\n", g_test_dbus_get_bus_address(dbus));
#else
    GError *error = NULL;
    GIOChannel *channel = NULL;
    gchar *bus_address = NULL;
    gint tmp_fd = 0;
    gint pipe_fd[2];
    gchar *argv[] = {"dbus-daemon", "--config-file=dbus.conf", "--print-address=<<fd>>", NULL};
    gsize len = 0;
    const gchar *dbus_monitor = NULL;

    if (pipe(pipe_fd)== -1) {
        g_warning("Failed to open temp file : %s", error->message);
        argv[2] = g_strdup_printf ("--print-address=1");
        g_spawn_async_with_pipes (NULL, argv, NULL, G_SPAWN_SEARCH_PATH, NULL, NULL, &daemon_pid, NULL, NULL, &tmp_fd, &error);
    } else {
        tmp_fd = pipe_fd[0];
        argv[2] = g_strdup_printf ("--print-address=%d", pipe_fd[1]);
        g_spawn_async (NULL, argv, NULL, G_SPAWN_SEARCH_PATH|G_SPAWN_LEAVE_DESCRIPTORS_OPEN, NULL, NULL, &daemon_pid, &error);
    }
    fail_if (error != NULL, "Failed to span daemon : %s", error ? error->message : "");
    fail_if (daemon_pid == 0, "Failed to get daemon pid");
    g_free (argv[2]);
    sleep (5); /* 5 seconds */

    channel = g_io_channel_unix_new (tmp_fd);
    g_io_channel_read_line (channel, &bus_address, NULL, &len, &error);
    fail_if (error != NULL, "Failed to daemon address : %s", error ? error->message : "");
    g_io_channel_unref (channel);
    
    if (pipe_fd[0]) close (pipe_fd[0]);
    if (pipe_fd[1]) close (pipe_fd[1]);

    if (bus_address) bus_address[len] = '\0';
    fail_if(bus_address == NULL || strlen(bus_address) == 0);

    fail_if (g_setenv("DBUS_SESSION_BUS_ADDRESS", bus_address, TRUE) == FALSE);

    g_print ("Daemon Address : %s\n", bus_address);
    g_free (bus_address);

    if ((dbus_monitor = g_getenv("DBUS_DEBUG")) != NULL && g_strcmp0 (dbus_monitor, "0")) {
    	/* start dbus-monitor */
    	char *argv[] = {"dbus-monitor", "--session", NULL };
    	g_spawn_async (NULL, argv, NULL, G_SPAWN_SEARCH_PATH, NULL, NULL, NULL, &error);
    	if (error) {
    		g_print ("Error while running dbus-monitor : %s", error->message);
    		g_error_free (error);
    	}
    }
#endif

    g_print ("Daemon PID = %d\n", daemon_pid);
}

static void
teardown_daemon (void)
{
#if HAVE_GTESTDBUS
    g_test_dbus_down (dbus);
#else
    if (daemon_pid) kill (daemon_pid, SIGTERM);
#endif
}

SSODbusUIDialog* _get_dialog_proxy (GError **error)
{
    gboolean res = FALSE;
    gchar *dialog_bus_address = NULL;
    SSODbusUI *ui_proxy = sso_dbus_ui_proxy_new_for_bus_sync (G_BUS_TYPE_SESSION, 
            G_DBUS_PROXY_FLAGS_DO_NOT_LOAD_PROPERTIES,
            "com.google.code.AccountsSSO.gSingleSignOn.UI", "/", NULL, error);

    if (ui_proxy == NULL) return NULL;

    res = sso_dbus_ui_call_get_bus_address_sync (ui_proxy, &dialog_bus_address, NULL, error);
    if (!dialog_bus_address) return NULL;

    GDBusConnection *connection = g_dbus_connection_new_for_address_sync (dialog_bus_address,
            G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_CLIENT,
            NULL, NULL, error);
    if (connection == NULL) return NULL;

    SSODbusUIDialog *dialog_proxy = sso_dbus_uidialog_proxy_new_sync (connection, 
            G_DBUS_PROXY_FLAGS_DO_NOT_LOAD_PROPERTIES,
            //"com.google.code.AccountsSSO.gSingleSignOn.UI.Dialog", 
            NULL,
            "/Dialog", NULL, error);
    g_free (dialog_bus_address);
    return dialog_proxy;
}

/*
 * Test cases
 */
START_TEST (test_dialog_service)
{
    GError *error = NULL;
    SSODbusUIDialog *proxy = _get_dialog_proxy (&error);
    fail_if (proxy == NULL, "Failed to get dialog proxy object - %s", error ? error->message : "");

    g_object_unref (proxy);
}
END_TEST

START_TEST (test_no_request_id)
{
    gboolean res = FALSE;
    GError *error = NULL;
    GVariant *reply = NULL;
    GHashTable *params = NULL;
    GVariant *val  = NULL;
    SSODbusUIDialog *proxy = _get_dialog_proxy (&error);
    fail_if (proxy == NULL, "Failed to get dialog proxy object - %s", error ? error->message : "");

    params = g_hash_table_new_full (g_str_hash, g_str_equal, NULL, (GDestroyNotify)g_variant_unref);
    g_hash_table_insert (params, GSSO_UI_KEY_QUERY_PASSWORD, g_variant_new_boolean (TRUE));
    res = sso_dbus_uidialog_call_query_dialog_sync (proxy, g_variant_map_from_hash_table(params), &reply, NULL, &error);

    g_hash_table_unref (params);
    fail_if (res == FALSE, "Failed to call query dialog : %s", error ? error->message : "");

    params = g_variant_map_to_hash_table (reply);
    val = g_hash_table_lookup (params, GSSO_UI_KEY_QUERY_ERROR_CODE);
    fail_if (val == NULL, "Expected ERROR code");

    fail_unless (g_variant_get_uint32 (val) == GSSO_UI_QUERY_ERROR_BAD_PARAMETERS);

    g_variant_unref (reply);
    g_hash_table_unref (params);
}
END_TEST

START_TEST (test_no_query)
{
    gboolean res = FALSE;
    GError *error = NULL;
    GVariant *reply = NULL;
    GHashTable *params = NULL;
    GVariant *val  = NULL;
    SSODbusUIDialog *proxy = _get_dialog_proxy (&error);
    fail_if (proxy == NULL, "Failed to get dialog proxy object - %s", error ? error->message : "");

    params = g_hash_table_new_full (g_str_hash, g_str_equal, NULL, (GDestroyNotify)g_variant_unref);
    g_hash_table_insert (params, GSSO_UI_KEY_REQUEST_ID, g_variant_new_string ("dummy_req"));
    res = sso_dbus_uidialog_call_query_dialog_sync (proxy, g_variant_map_from_hash_table(params), &reply, NULL, &error);

    g_hash_table_unref (params);
    fail_if (res == FALSE, "Failed to call query dialog : %s", error ? error->message : "");

    params = g_variant_map_to_hash_table (reply);
    val = g_hash_table_lookup (params, GSSO_UI_KEY_QUERY_ERROR_CODE);
    fail_if (val == NULL, "Expected ERROR code");

    fail_unless (g_variant_get_uint32 (val) == GSSO_UI_QUERY_ERROR_BAD_PARAMETERS);

    g_variant_unref (reply);
    g_hash_table_unref (params);
}
END_TEST

START_TEST(test_query_password)
{
    gboolean res = FALSE;
    GError *error = NULL;
    GVariant *reply = NULL;
    GHashTable *params = NULL;
    GVariant *val  = NULL;

    SSODbusUIDialog *proxy = _get_dialog_proxy (&error);
    fail_if (proxy == NULL, "Failed to get dialog proxy object - %s", error ? error->message : "");

    params = g_hash_table_new_full (g_str_hash, g_str_equal, NULL, (GDestroyNotify)g_variant_unref);
    g_hash_table_insert (params, GSSO_UI_KEY_REQUEST_ID, g_variant_new_string ("dummy_req"));
    g_hash_table_insert (params, GSSO_UI_KEY_QUERY_PASSWORD, g_variant_new_boolean(TRUE));
    g_hash_table_insert (params, GSSO_UI_KEY_TEST_REPLY_VALUES, 
            g_variant_new_string(GSSO_UI_KEY_PASSWORD":12345"));
    res = sso_dbus_uidialog_call_query_dialog_sync (proxy, g_variant_map_from_hash_table(params), &reply, NULL, &error);

    g_hash_table_unref (params);
    fail_if (res == FALSE, "Failed to call query dialog : %s", error ? error->message : "");

    params = g_variant_map_to_hash_table (reply);
    val = g_hash_table_lookup (params, GSSO_UI_KEY_QUERY_ERROR_CODE);
    fail_if (g_variant_get_uint32 (val) != GSSO_UI_QUERY_ERROR_NONE, "Got Error: %d", g_variant_get_uint32 (val));

    val = g_hash_table_lookup (params, GSSO_UI_KEY_PASSWORD);
    fail_if (val == NULL, "Failed to get secret");

    fail_if (g_strcmp0 (g_variant_get_string (val, NULL), "12345") != 0);

    g_variant_unref (reply);
    g_hash_table_unref (params);
}
END_TEST

START_TEST (test_query_remember_password)
{
    gboolean res = FALSE;
    GError *error = NULL;
    GVariant *reply = NULL;
    GHashTable *params = NULL;
    GVariant *val  = NULL;

    SSODbusUIDialog *proxy = _get_dialog_proxy (&error);
    fail_if (proxy == NULL, "Failed to get dialog proxy object - %s", error ? error->message : "");

    params = g_hash_table_new_full (g_str_hash, g_str_equal, NULL, (GDestroyNotify)g_variant_unref);
    g_hash_table_insert (params, GSSO_UI_KEY_REQUEST_ID, g_variant_new_string ("dummy_req"));
    g_hash_table_insert (params, GSSO_UI_KEY_QUERY_PASSWORD, g_variant_new_boolean(TRUE));
    g_hash_table_insert (params, GSSO_UI_KEY_REMEMBER_PASSWORD, g_variant_new_boolean (FALSE));
    g_hash_table_insert (params, GSSO_UI_KEY_TEST_REPLY_VALUES,
        g_variant_new_string(GSSO_UI_KEY_PASSWORD":12345,"GSSO_UI_KEY_REMEMBER_PASSWORD":True"));
    res = sso_dbus_uidialog_call_query_dialog_sync (proxy, g_variant_map_from_hash_table(params), &reply, NULL, &error);

    g_hash_table_unref (params);
    fail_if (res == FALSE, "Failed to call query dialog : %s", error ? error->message : "");

    params = g_variant_map_to_hash_table (reply);
    val = g_hash_table_lookup (params, GSSO_UI_KEY_QUERY_ERROR_CODE);
    fail_if (g_variant_get_uint32 (val) != GSSO_UI_QUERY_ERROR_NONE, "Got Error: %d", g_variant_get_uint32 (val));

    val = g_hash_table_lookup (params, GSSO_UI_KEY_PASSWORD);
    fail_if (val == NULL, "Failed to get secret");
    fail_if (g_strcmp0 (g_variant_get_string (val, NULL), "12345") != 0);
    
    val = g_hash_table_lookup (params, GSSO_UI_KEY_REMEMBER_PASSWORD);
    fail_if (val == NULL, "Failed to get secret");
    fail_if (g_variant_get_boolean(val) != TRUE);

    g_variant_unref (reply);
    g_hash_table_unref (params);
}
END_TEST

START_TEST(test_query_username_and_password)
{
    gboolean res = FALSE;
    GError *error = NULL;
    GVariant *reply = NULL;
    GHashTable *params = NULL;
    GVariant *val  = NULL;

    SSODbusUIDialog *proxy = _get_dialog_proxy (&error);
    fail_if (proxy == NULL, "Failed to get dialog proxy object - %s", error ? error->message : "");

    params = g_hash_table_new_full (g_str_hash, g_str_equal, NULL, (GDestroyNotify)g_variant_unref);
    g_hash_table_insert (params, GSSO_UI_KEY_REQUEST_ID, g_variant_new_string ("dummy_req"));
    g_hash_table_insert (params, GSSO_UI_KEY_QUERY_USERNAME, g_variant_new_boolean(TRUE));
    g_hash_table_insert (params, GSSO_UI_KEY_QUERY_PASSWORD, g_variant_new_boolean(TRUE));
    g_hash_table_insert (params, GSSO_UI_KEY_TEST_REPLY_VALUES, 
        g_variant_new_string(GSSO_UI_KEY_PASSWORD":12345,"GSSO_UI_KEY_USERNAME":uname"));
    res = sso_dbus_uidialog_call_query_dialog_sync (proxy, g_variant_map_from_hash_table(params), &reply, NULL, &error);

    g_hash_table_unref (params);
    fail_if (res == FALSE, "Failed to call query dialog : %s", error ? error->message : "");

    params = g_variant_map_to_hash_table (reply);
    val = g_hash_table_lookup (params, GSSO_UI_KEY_QUERY_ERROR_CODE);
    fail_if (g_variant_get_uint32 (val) != GSSO_UI_QUERY_ERROR_NONE, "Got Error: %d", g_variant_get_uint32 (val));

    val = g_hash_table_lookup (params, GSSO_UI_KEY_USERNAME);
    fail_if (val == NULL, "Failed to get username");
    fail_if (g_strcmp0 (g_variant_get_string (val, NULL), "uname") != 0);
    
    val = g_hash_table_lookup (params, GSSO_UI_KEY_PASSWORD);
    fail_if (val == NULL, "Failed to get secret");
    fail_if (g_strcmp0 (g_variant_get_string (val, NULL), "12345") != 0);
 
    g_variant_unref (reply);
    g_hash_table_unref (params);
}
END_TEST

START_TEST(test_query_confirm)
{
    gboolean res = FALSE;
    GError *error = NULL;
    GVariant *reply = NULL;
    GHashTable *params = NULL;
    GVariant *val  = NULL;

    SSODbusUIDialog *proxy = _get_dialog_proxy (&error);
    fail_if (proxy == NULL, "Failed to get dialog proxy object - %s", error ? error->message : "");

    params = g_hash_table_new_full (g_str_hash, g_str_equal, NULL, (GDestroyNotify)g_variant_unref);
    g_hash_table_insert (params, GSSO_UI_KEY_REQUEST_ID, g_variant_new_string ("dummy_req"));
    g_hash_table_insert (params, GSSO_UI_KEY_CONFIRM, g_variant_new_boolean(TRUE));
    g_hash_table_insert (params, GSSO_UI_KEY_USERNAME, g_variant_new_string ("dummy_user"));
    g_hash_table_insert (params, GSSO_UI_KEY_PASSWORD, g_variant_new_string("12345"));
    g_hash_table_insert (params, GSSO_UI_KEY_TEST_REPLY_VALUES, g_variant_new_string(GSSO_UI_KEY_CONFIRM_SECRET":67890"));
    res = sso_dbus_uidialog_call_query_dialog_sync (proxy, g_variant_map_from_hash_table(params), &reply, NULL, &error);

    g_hash_table_unref (params);
    fail_if (res == FALSE, "Failed to call query dialog : %s", error ? error->message : "");

    params = g_variant_map_to_hash_table (reply);
    val = g_hash_table_lookup (params, GSSO_UI_KEY_QUERY_ERROR_CODE);
    fail_if (g_variant_get_uint32 (val) != GSSO_UI_QUERY_ERROR_NONE, "Got Error: %d", g_variant_get_uint32 (val));

    val = g_hash_table_lookup (params, GSSO_UI_KEY_PASSWORD);
    fail_if (val == NULL, "Failed to get confirm/new secret");
    fail_if (g_strcmp0 (g_variant_get_string (val, NULL), "67890") != 0);
 
    g_variant_unref (reply);
    g_hash_table_unref (params);
}
END_TEST

START_TEST(test_query_captcha)
{
    gboolean res = FALSE;
    GError *error = NULL;
    GVariant *reply = NULL;
    GHashTable *params = NULL;
    GVariant *val  = NULL;

    SSODbusUIDialog *proxy = _get_dialog_proxy (&error);
    fail_if (proxy == NULL, "Failed to get dialog proxy object - %s", error ? error->message : "");

    params = g_hash_table_new_full (g_str_hash, g_str_equal, NULL, (GDestroyNotify)g_variant_unref);
    g_hash_table_insert (params, GSSO_UI_KEY_REQUEST_ID, g_variant_new_string ("dummy_req"));
    g_hash_table_insert (params, GSSO_UI_KEY_QUERY_PASSWORD, g_variant_new_boolean(TRUE));
    g_hash_table_insert (params, GSSO_UI_KEY_USERNAME, g_variant_new_string ("dummy_user"));
    g_hash_table_insert (params, GSSO_UI_KEY_CAPTCHA_URL, g_variant_new_string("file:///usr/share/locale/l10n/us/flag.png"));
    g_hash_table_insert (params, GSSO_UI_KEY_TEST_REPLY_VALUES, g_variant_new_string(GSSO_UI_KEY_CAPTCHA_RESPONSE":qwerty"));
    res = sso_dbus_uidialog_call_query_dialog_sync (proxy, g_variant_map_from_hash_table(params), &reply, NULL, &error);

    g_hash_table_unref (params);
    fail_if (res == FALSE, "Failed to call query dialog : %s", error ? error->message : "");

    params = g_variant_map_to_hash_table (reply);
    val = g_hash_table_lookup (params, GSSO_UI_KEY_QUERY_ERROR_CODE);
    fail_if (g_variant_get_uint32 (val) != GSSO_UI_QUERY_ERROR_NONE, "Got Error: %d", g_variant_get_uint32 (val));

    val = g_hash_table_lookup (params, GSSO_UI_KEY_CAPTCHA_RESPONSE);
    fail_if (val == NULL, "Failed to get confirm/new secret");
    fail_if (g_strcmp0 (g_variant_get_string (val, NULL), "qwerty") != 0);
 
    g_variant_unref (reply);
    g_hash_table_unref (params);
}
END_TEST

START_TEST(test_query_oauth)
{
    gboolean res = FALSE;
    GError *error = NULL;
    GVariant *reply = NULL;
    GHashTable *params = NULL;
    GVariant *val  = NULL;

    SSODbusUIDialog *proxy = _get_dialog_proxy (&error);
    fail_if (proxy == NULL, "Failed to get dialog proxy object - %s", error ? error->message : "");

    params = g_hash_table_new_full (g_str_hash, g_str_equal, NULL, (GDestroyNotify)g_variant_unref);
    g_hash_table_insert (params, GSSO_UI_KEY_REQUEST_ID, g_variant_new_string ("dummy_req"));
    g_hash_table_insert (params, GSSO_UI_KEY_OPEN_URL, g_variant_new_string("http://?param=val&param2=val2"));
    g_hash_table_insert (params, GSSO_UI_KEY_FINAL_URL, g_variant_new_string ("http://"));
    g_hash_table_insert (params, GSSO_UI_KEY_TEST_REPLY_VALUES,
            g_variant_new_string(GSSO_UI_KEY_URL_RESPONSE":param=val&param2=val2"));
    res = sso_dbus_uidialog_call_query_dialog_sync (proxy, g_variant_map_from_hash_table(params), &reply, NULL, &error);

    g_hash_table_unref (params);
    fail_if (res == FALSE, "Failed to call query dialog : %s", error ? error->message : "");

    params = g_variant_map_to_hash_table (reply);

    val = g_hash_table_lookup (params, GSSO_UI_KEY_QUERY_ERROR_CODE);
    fail_if (val == NULL, "Failed to fetch error code");
    fail_if (g_variant_get_uint32 (val) != GSSO_UI_QUERY_ERROR_NONE, "Got Error: %d", g_variant_get_uint32 (val));

    val = g_hash_table_lookup (params, GSSO_UI_KEY_URL_RESPONSE);
    fail_if (val == NULL, "Failed to get URL response");
    fail_if (g_strcmp0 (g_variant_get_string (val, NULL), "param=val&param2=val2") != 0);
 
    g_variant_unref (reply);
    g_hash_table_unref (params);
}
END_TEST

START_TEST(test_cancel)
{
    gboolean res = FALSE;
    GError *error = NULL;
    GVariant *reply = NULL;
    GHashTable *params = NULL;
    GVariant *val  = NULL;
    gchar cancel_reply[128];

    g_sprintf (cancel_reply, "%s:%d", GSSO_UI_KEY_QUERY_ERROR_CODE, GSSO_UI_QUERY_ERROR_CANCELED);

    SSODbusUIDialog *proxy = _get_dialog_proxy (&error);
    fail_if (proxy == NULL, "Failed to get dialog proxy object - %s", error ? error->message : "");

    params = g_hash_table_new_full (g_str_hash, g_str_equal, NULL, (GDestroyNotify)g_variant_unref);
    g_hash_table_insert (params, GSSO_UI_KEY_REQUEST_ID, g_variant_new_string ("dummy_req"));
    g_hash_table_insert (params, GSSO_UI_KEY_QUERY_PASSWORD, g_variant_new_boolean(TRUE));
    g_hash_table_insert (params, GSSO_UI_KEY_TEST_REPLY_VALUES,
            g_variant_new_string(cancel_reply));
    res = sso_dbus_uidialog_call_query_dialog_sync (proxy, g_variant_map_from_hash_table(params), &reply, NULL, &error);

    g_hash_table_unref (params);
    fail_if (res == FALSE, "Failed to call query dialog : %s", error ? error->message : "");

    params = g_variant_map_to_hash_table (reply);

    val = g_hash_table_lookup (params, GSSO_UI_KEY_QUERY_ERROR_CODE);
    fail_if (val == NULL, "Failed to fetch error code");
    fail_if (g_variant_get_uint32 (val) != GSSO_UI_QUERY_ERROR_CANCELED,
            "Expected 'cancele' but Got Error: %d", g_variant_get_uint32 (val));

    g_variant_unref (reply);
    g_hash_table_unref (params);
}
END_TEST

Suite* daemon_suite (void)
{
    Suite *s = suite_create ("GSignon UI daemon tests");
    
    TCase *tc = tcase_create ("DIalog");

    tcase_set_timeout(tc, 10);
    tcase_add_unchecked_fixture (tc, setup_daemon, teardown_daemon);

    tcase_add_test (tc, test_dialog_service);
    tcase_add_test (tc, test_no_request_id);
    tcase_add_test (tc, test_no_query);
    tcase_add_test (tc, test_query_password);
    tcase_add_test (tc, test_query_remember_password);
    tcase_add_test (tc, test_query_username_and_password);
    tcase_add_test (tc, test_query_confirm);
    //tcase_add_test (tc, test_query_captcha);
    tcase_add_test (tc, test_query_oauth);
    tcase_add_test (tc, test_cancel);

    suite_add_tcase (s, tc);

    return s;
}

int main (int argc, char *argv[])
{
    int number_failed;
    Suite *s = 0;
    SRunner *sr = 0;
   
#if !GLIB_CHECK_VERSION (2, 36, 0)
    g_type_init ();
#endif

    s = daemon_suite();
    sr = srunner_create(s);

    srunner_run_all(sr, CK_VERBOSE);

    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
