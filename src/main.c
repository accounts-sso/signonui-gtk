/*
 * This file is part of signonui-gtk
 *
 * Copyright (C) 2013 Intel Corporation.
 *
 * Author: Amarnath Valluri <amarnath.valluri@intel.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <glib.h>
#include <glib-unix.h>
#include <gtk/gtk.h>
#include "gsso-ui-server.h"
#include "gsso-ui-log.h"

#ifndef DAEMON_TIMEOUT
#   define DAEMON_TIMEOUT (10) // seconds
#endif

guint _unix_signal_source_id[3]; /* SIGTERM, SIGHUP, SIGINT */

static gboolean
_on_unix_signal (gpointer __attribute__ ((unused)) data)
{
    gtk_main_quit ();
    return FALSE;
}

static void
_setup_signal_handlers ()
{
    GSource *source = NULL;

    source = g_unix_signal_source_new (SIGTERM);
    g_source_set_callback (source, _on_unix_signal, NULL, NULL);
    _unix_signal_source_id[0] = g_source_attach (source, NULL);

    source = g_unix_signal_source_new (SIGHUP);
    g_source_set_callback (source, _on_unix_signal, NULL, NULL);
    _unix_signal_source_id[1] = g_source_attach (source, NULL);

    source = g_unix_signal_source_new (SIGINT);
    g_source_set_callback (source, _on_unix_signal, NULL, NULL);
    _unix_signal_source_id[2] = g_source_attach (source, NULL);
}

static void 
_on_server_closed (gpointer data, GObject *dead_obj)
{
    DBG ("==== SERVER CLOSED");
    if (data) *(GSSOUIServer **)data = NULL;
    gtk_main_quit ();
}

int main (int argc, char **argv)
{
    GSSOUIServer *server = NULL;
    gboolean keep_running = FALSE;
    const char *env_timeout = NULL;
    guint32 timeout = 0;
    gboolean res = FALSE;
    GError *error = NULL;
    
    GOptionContext *opt_context = NULL;
    GOptionEntry opt_entries[] = {
        { "keep-running", 0, 0, G_OPTION_ARG_NONE, &keep_running, "Keep running", "disable default timer"},
        {NULL }
    };

    opt_context = g_option_context_new ("SSO daemon");
    g_option_context_add_main_entries (opt_context, opt_entries, NULL);
    res = g_option_context_parse (opt_context, &argc, &argv, &error);
    g_option_context_free (opt_context);
    if(!res) {
        WARN ("Error parsing options: %s", error->message);
        g_error_free (error);
        return -1;
    }

    gtk_init (&argc, &argv);

    _setup_signal_handlers (NULL);
#ifdef ENABLE_TIMEOUT
    env_timeout = g_getenv("GSSO_UI_TIMEOUT");
    if (env_timeout) timeout = atoi (env_timeout);
    if (!timeout) timeout = DAEMON_TIMEOUT;
#endif
    server = gsso_ui_server_new (keep_running ? 0 : timeout);
    g_object_weak_ref (G_OBJECT(server), _on_server_closed, &server);

    gtk_main();

    if (server) {
        g_object_weak_unref (G_OBJECT(server), _on_server_closed, &server);
        g_object_unref (server);
    }

    DBG ("Clean shutdown");

    return 0;
}

